<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Contracts;

interface DataArrayModel
{
    public function setData($key, $value = null);
    public function getData($key = null, $default = null);
    public function hasData($key);
}
