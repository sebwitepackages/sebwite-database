<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Contracts;

/**
 * This is the class SortableModel.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
interface SortingRepository
{

    public function reorder(array $ids, $startOrder = 1);

    public function sort($order, $whereId = false);
}
