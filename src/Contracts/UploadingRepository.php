<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Contracts;

/**
 * This is the UploadingRepository.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
interface UploadingRepository
{

    /**
     * uploader
     *
     * @return \Sebwite\Upload\Uploader
     */
    public function uploader();

    /**
     * hasUploadingFiles
     *
     * @return bool
     */
    public function hasUploadingFiles();

    /**
     * getUploadingFields
     *
     * @return array
     */
    public function getUploadingFields();
}
