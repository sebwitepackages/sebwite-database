<?php

namespace Sebwite\Database;

use Sebwite\Support\ServiceProvider;

/**
* The main service provider
*
* @author        Sebwite
* @copyright  Copyright (c) 2015, Sebwite
* @license      https://tldrlegal.com/license/mit-license MIT
* @package      Sebwite\Database
*/
class DatabaseServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'sebwite.database' ];


}
