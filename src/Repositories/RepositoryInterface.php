<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Repositories;

/**
 * This is the class RepositoryInterface.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
interface RepositoryInterface
{
    public function createModel(array $data = []);

    public function model(array $data = []);

    public function getNamespace();

    /**
     * getModel method
     *
     * @return string|\Illuminate\Database\Eloquent\Model
     */
    public function getModel();


    /**
     * Returns a dataset compatible with data grid.
     *
     * @return \Ccblearning\Courses\Database\Course
     */
    public function grid();

    /**
     * Returns all the courses entries.
     *
     * @return \Ccblearning\Courses\Database\Course
     */
    public function findAll();

    /**
     * Returns a courses entry by its primary key.
     *
     * @param  int  $id
     *
*@return \Ccblearning\Courses\Database\Course
     */
    public function find($id);

    /**
     * Determines if the given courses is valid for creation.
     *
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForCreation(array $data);

    /**
     * Determines if the given courses is valid for update.
     *
     * @param  int  $id
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForUpdate($id, array $data);


    public function delete($id);
}
