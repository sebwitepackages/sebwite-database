<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Repositories;

use Ccblearning\Support\Traits;

/**
 * This is the AbstractRepository.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 *
 * @mixin \Illuminate\Database\Eloquent\Model
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
abstract class AbstractRepository
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;


    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model(array $data = [])
    {
        return $this->createModel($data);
    }

    public function getNamespace()
    {
        return property_exists($this, 'namespace') ? $this->namespace : $this->getModel();
    }

    public function grid()
    {
        return $this->createModel();
    }

    public function findAll()
    {
        return $this->createModel()->get();
    }

    /**
     * find method
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }

    public function validForUpdate($id, array $input)
    {
        return $this->validator->on('update')->validate($input);
    }


    public function delete($id)
    {
        if ($content = $this->find($id)) {
            $this->fireEvent($this->getNamespace() . '.deleted', [ $content ]);
            $content->delete();

            return true;
        }

        return false;
    }

    public function enable($id)
    {
        $this->validator->bypass();

        return $this->update($id, [ 'enabled' => true ]);
    }

    public function disable($id)
    {
        $this->validator->bypass();

        return $this->update($id, [ 'enabled' => false ]);
    }
}
