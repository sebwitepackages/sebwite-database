<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

interface TypeManager
{
    public function getTypes();

    public function getType($type);

    /**
     * hasType method
     *
     * @param $type
     *
     * @return bool
     */
    public function hasType($type);

    public function registerType(Type $type);
}
