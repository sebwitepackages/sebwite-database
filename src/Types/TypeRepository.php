<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

interface TypeRepository
{

    /**
     * getTypeManager method
     *
     * @return mixed
     * @throws \ErrorException
     */
    public function getTypeManager();

    /**
     * {@inheritDoc}
     */
    public function getTypes();
}
