<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

trait TypeRepositoryTrait
{
    /**
     * getTypeManager method
     *
     * @return \Sebwite\Database\Types\TypeManager
     * @throws \ErrorException
     */
    public function getTypeManager()
    {
        $model = $this->createModel();
        if (!$model instanceof TypeModel) {
            throw new \ErrorException("Model is not a TypeInterface");
        }

        return $model->getTypeManager();
    }

    /**
     * getTypes method
     *
     * @return mixed
     * @throws \ErrorException
     */
    public function getTypes()
    {
        return $this->getTypeManager()->getTypes();
    }
}
