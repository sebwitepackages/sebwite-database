<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

interface TypeModel
{
    public function getTypeManagerBinding();

    public function getTypeColumn();

    /**
     * getTypeManager method
     *
     * @return TypeManager
     */
    public function getTypeManager();

    /**
     * getType method
     *
     * @return TypeModel
     */
    public function getType();
}
