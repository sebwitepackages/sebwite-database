<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Translation\Translator;
use Sebwite\Support\Collection;

abstract class BaseType implements Type
{
    /**
     * The container instance.
     *
     * @var \Illuminate\Container\Container
     */
    protected $app;

    /**
     * The View Factory.
     *
     * @var \Illuminate\View\Factory
     */
    protected $view;

    /**
     * @var \Cartalyst\Themes\Assets\AssetManager
     */
    protected $assets;

    /**
     * @var \Ccblearning\Exams\Database\QuestionRepository
     */
    protected $repository;

    /**
     * @var \Ccblearning\Exams\Type\QuestionTypeManager
     */
    protected $manager;

    /**
     * @var \Ccblearning\Exams\ExamValidator
     */
    protected $validator;

    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $rules;

    /**
     * @var UrlGenerator
     */
    protected $url;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * Extra variable data
     * @var \Sebwite\Support\Collection
     */
    protected $context;

    /**
     * AbstractPageType constructor.
     *
     * @param \Illuminate\Container\Container $app
     * @param \Illuminate\Http\Request        $request
     */
    public function __construct(Container $app, Request $request)
    {
        $this->app = $app;

        $this->request = $request;

        $this->view = $app[ 'view' ];

        $this->assets = $app[ 'theme.assets' ];

        $this->url = $app[ 'url' ];

        $this->translator = $app[ 'translator' ];

        $this->context = new Collection;

        $this->rules = [ ];

        $this->onCreate();
    }

    public function onCreate()
    {

    }

    public function validate($model = null, $on, array $data = [ ])
    {
        $rules = array_replace_recursive($this->validator->getRules(), $this->rules);

        return $this->validator->setRules($rules)->on($on)->validate($data);
    }

    /** @noinspection PhpDocSignatureInspection */
    /**
     * trans method
     *
     * @param        $id
     * @param array  $params
     * @param string $domain
     * @param null   $locale
     *
     * @return mixed
     */
    public function trans()
    {
        return call_user_func_array([$this->translator, 'trans'], func_get_args());
    }

    /**
     * @return \Illuminate\View\Factory
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return \Cartalyst\Themes\Assets\AssetManager
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @return \Ccblearning\Exams\Database\QuestionRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set the repository value
     *
     * @param \Ccblearning\Exams\Database\QuestionRepository $repository
     *
     * @return BaseType
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * @return \Ccblearning\Exams\Type\QuestionTypeManager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set the manager value
     *
     * @param \Ccblearning\Exams\Type\QuestionTypeManager $manager
     *
     * @return BaseType
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return \Sebwite\Support\Validator
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set the validator value
     *
     * @param \Ccblearning\Exams\ExamValidator $validator
     *
     * @return BaseType
     */
    public function setValidator($validator)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Set the rules value
     *
     * @param array $rules
     *
     * @return BaseType
     */
    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * @return UrlGenerator
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the url value
     *
     * @param UrlGenerator $url
     *
     * @return BaseType
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * Set the translator value
     *
     * @param Translator $translator
     *
     * @return BaseType
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;

        return $this;
    }

    public function context()
    {
        return $this->context;
    }
}
