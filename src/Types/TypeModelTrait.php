<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

/**
 * This is the class ManagedTypeTrait.
 *
 * @package        Sebwite\Database
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @mixin \Illuminate\Database\Eloquent\Model
 */
trait TypeModelTrait
{
    abstract public function getTypeManagerBinding();

    abstract public function getTypeColumn();

    /**
     * getTypeManager method
     *
     * @return TypeManager|BaseTypeManager|mixed
     */
    public function getTypeManager()
    {
        return app($this->getTypeManagerBinding());
    }

    public function getTypeIdentifier($typeClass = null)
    {

        $typeClass = $typeClass ?: $this->getAttribute($this->getTypeColumn());
        return strtolower(last(explode('\\', $typeClass)));
    }


    /**
     * getType method
     *
     * @return \Ccblearning\Courses\PageType\PageTypeInterface|\Ccblearning\Courses\PageType\Types\TextPageType
     */
    public function getType()
    {
        return $this->getTypeManager()->getType($this->getTypeIdentifier());
    }

    /**
     * Handle dynamic method calls into the method.
     *
     * @param  string $method
     * @param  array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (!app()->bound($this->getTypeManagerBinding()) || !$this instanceof TypeModel) {
            return parent::__call($method, $parameters);
        }

        $type = $this->getType();

        $methodFound = false;

        $_method = 'get' . substr($method, 3) . 'Attribute';

        if (method_exists($type, $method)) {
            $methodFound = true;
        } elseif (method_exists($type, $_method)) {
            $method = $_method;

            $methodFound = true;
        }

        if ($methodFound) {
            array_unshift($parameters, $this);

            return call_user_func_array([ $type, $method ], $parameters);
        }

        return parent::__call($method, $parameters);
    }
}
