<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Types;

abstract class BaseTypeManager implements TypeManager
{
    protected $types = [];

    public function getTypes()
    {
        return $this->types;
    }

    /**
     * {@inheritDoc}
     */
    public function getType($type)
    {
        if (array_key_exists($type, $this->types)) {
            return $this->types[$type];
        }
    }

    /**
     * registerType method
     *
     * @param \Sebwite\Database\Types\Type $type
     */
    public function registerType(Type $type)
    {
        $this->types[$type->getIdentifier()] = $type;
    }

    public function hasType($type)
    {
        if ($type instanceof Type) {
            $type = $type->getIdentifier();
        }
        return array_key_exists($type, $this->types);
    }
}
