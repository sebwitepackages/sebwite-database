<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Traits;

use Ccblearning\Support\Database\ImageModelInterface;
use Symfony\Component\HttpFoundation\File\File;
/**
 * This is the UploadingModuleTrait.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 * @mixin \Sebwite\Database\Repositories\AbstractRepository
 */
trait UploadingRepositoryTrait
{
    public abstract function getFileFields();

    /**
     * uploader
     *
     * @return \Sebwite\Upload\Uploader
     */
    public function uploader()
    {
        return app('sebwite.upload')->make($this->getFileFields());
    }

    public function checkImageUpload(&$input, ImageModelInterface $model, array $config)
    {
        $field = $model->getImageColumn();

        if ( array_key_exists($field, $input) && $input[ $field ] !== $model[ $field ] && $model[ $field ] !== null ) {
            $fs       = app('fs');
            $filePath = public_path($model[ $field ]);
            $fs->isFile($filePath) && $fs->delete($filePath);
        }

        if ( $this->hasUploadingFiles() ) {
            $upload = $this->uploader()
                ->rename($field)
                ->transform($field, function (File $file) use ($config) {
                    $image = app('image')->make($file->getPathname());
                    $height = $config[ 'image-fit-height' ] === false ? $image->getHeight() : $config[ 'image-fit-height' ];
                    $image->fit($config[ 'image-fit-width' ], $height, function($constraints) {
                        $constraints->upsize();
                    });
                    $image->save();
                    return $file;
                })
                ->upload($config[ 'image-upload-path' ]);

            if ( !$upload->isEmpty() && !$upload->fails() ) {
                $input[ $field ] = $upload->getFileRelativePath($field, public_path());
            }
        }

        return $input;
    }

    /**
     * hasUploadingFiles
     *
     * @return bool
     */
    public function hasUploadingFiles()
    {
        foreach ( $this->getFileFields() as $field ) {
            if ( request()->files->has($field) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * getUploadingFields
     *
     * @return array
     */
    public function getUploadingFields()
    {
        $fields = [ ];
        foreach ( $this->getFileFields() as $field ) {
            if ( request()->files->has($field) ) {
                $fields[] = $field;
            }
        }

        return $fields;
    }
}
