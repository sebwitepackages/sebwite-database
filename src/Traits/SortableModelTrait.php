<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Traits;

use Illuminate\Database\Eloquent\Model;
use Sebwite\Database\Contracts\SortableModel;

/**
 * This is the class SortableModelTrait.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @mixin \Illuminate\Database\Eloquent\Model
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Illuminate\Database\Query\Builder
 */
trait SortableModelTrait
{

    public static function bootSortableModelTrait()
    {
        static::creating(function (SortableModel $model) {

            /**
             * @var \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder $model
             */
            if ($model->getSortableOption('on_create') === true) {
                $column = $model->getSortableOption('column');
                $where  = $model->getSortableOption('where');
                $query  = $model->newQueryWithoutScopes();

                if ($where !== false) {
                    $query = call_user_func_array([ $query, 'where' ], array_merge($where, [$model->{$where[0]}]));
                }
                $max = (int)$query->max($column) ;

                if ($query->get()->count() > 0) {
                    $max++;
                }
                $model->setAttribute($column, $max);
            }
        });
    }


    public function getSortableOption($key)
    {
        $defaultOptions = [
            'column'    => 'position',
            'where'     => false, // false or ['columnName', '=']
            'on_create' => true
        ];
        $hasOptions     = property_exists($this, 'sortableOptions') && is_array($this->sortableOptions);

        return array_get(array_replace_recursive($defaultOptions, $hasOptions ? $this->sortableOptions : [ ]), $key);
    }



    public function scopeSorted($query, $value = null)
    {
        $where = $this->getSortableOption('where');
        if ($where !== false && !is_null($value)) {
            if ($value instanceof Model) {
                $value = $value->id;
            }

            $query = call_user_func_array([ $query, 'where' ], array_merge($where, [ $value ]));
        }
        $query->orderBy($this->getSortableOption('column'), 'asc');

        return $query;
    }
}
