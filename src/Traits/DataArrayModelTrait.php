<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Traits;

trait DataArrayModelTrait
{
    abstract public function getDataArrayColumn();

    public function setData($key, $value = null)
    {
        $col = $this->getDataArrayColumn();
        $data       = $this->{$col};
        $this->{$col} = is_array($key) ? $key : array_set($data, $key, $value);
    }

    /**
     * getData
     *
     * @param null $key
     * @param null $default
     *
     * @return mixed
     */
    public function getData($key = null, $default = null)
    {
        $col = $this->getDataArrayColumn();
        return $key === null ? $this->{$col} : array_get($this->{$col}, $key, $default);
    }

    public function hasData($key)
    {
        $col = $this->getDataArrayColumn();
        return array_has($this->{$col}, $key);
    }
}
