<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Database\Traits;

use Illuminate\Database\Eloquent\Model;
use Sebwite\Database\Contracts\SortableModel;

/**
 * This is the class SortingTrait.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
trait SortingRepositoryTrait
{

    /**
     * reorder method
     *
     * @param array $ids
     * @param int   $startOrder
     * @deprecated This doesn't work, should use sort() instead
     */
    public function reorder(array $ids, $startOrder = 1)
    {
        $model = $this->createModel();
        if (! $model instanceof SortableModel) {
            return;
        }
        $column = $model->getSortableOption('column');
        #$where = $model->getSortableOption('where');
        foreach ($ids as $id) {
            $model          = $this->find($id);
            $model->$column = $startOrder++;
            $model->save();
        }
    }

    public function sort($order, $whereId = false)
    {
        $model = $this->createModel();
        if (! $model instanceof SortableModel) {
            return;
        }

        $column = $model->getSortableOption('column');
        $where  = $model->getSortableOption('where');

        # get valid ID's
        $query = $this->createModel()->select([ 'id' ]);
        if ($whereId !== false && $where !== false) {
            if ($whereId instanceof Model) {
                $whereId = $whereId->id;
            }
            $query = call_user_func_array([ $query, 'where' ], array_merge($where, [$whereId]));
        }
        $validIds = $query->get()
            ->pluck('id')
            ->transform(function ($v) {

                return (int)$v;
            })
            ->toArray();

        foreach ($order as $position => $id) {
            if (! in_array((int)$id, $validIds, true)) {
                continue;
            }
            $record = $this->createModel()->find((int)$id);
            $record->{$column} = $position;
            $record->save();
        }
    }
}
