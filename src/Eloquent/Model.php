<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Eloquent;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Sebwite\Support\Collection as BaseCollection;

class Model extends BaseModel
{

    /**
     * newCollection method
     *
     * @param array $models
     *
     * @return \Sebwite\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }

    protected function castAttribute($key, $value)
    {
        $value = parent::castAttribute($key, $value);

        if ($key === 'collection') {
            return new BaseCollection($this->fromJson($value));
        }
        return $value;
    }
}
