<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Database\Eloquent;

use Sebwite\Support\Collection as BaseCollection;
use Sebwite\Database\Traits\CollectionTrait;

/**
 * This is the class Collection.
 *
 * @package        Sebwite\Database
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class Collection extends \Illuminate\Database\Eloquent\Collection
{
    use CollectionTrait;

    /**
     * Get a base Support collection instance from this collection.
     *
     * @return \Sebwite\Support\Collection
     */
    public function toBase()
    {
        return new BaseCollection($this->items);
    }
}
