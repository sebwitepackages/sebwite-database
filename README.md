Sebwite Database
====================

[![Build Status](https://img.shields.io/travis/sebwite/database.svg?&style=flat-square)](https://travis-ci.org/sebwite/database)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/database.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/database)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/database.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/database)
[![Source](http://img.shields.io/badge/source-sebwite/database-blue.svg?style=flat-square)](https://github.com/sebwite/database)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

Sebwite Database is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Documentation
-------------
Tbd

Quick Installation
------------------
Begin by installing the package through Composer.

```bash
composer require sebwite/database
```

